\input{header.tex}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{float}
\usepackage{indentfirst}
\usepackage{vmargin}
\usepackage{indentfirst}
\usepackage{titling}
\usepackage{color} 
\usepackage{siunitx}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage[backend=biber,backref=true,style=unsrt,
style=numeric-comp,block=ragged,firstinits=true]{biblatex}

%macro for column vector notation
\makeatletter
\newcommand{\Spvek}[2][r]{%
  \gdef\@VORNE{1}
  \left(\hskip-\arraycolsep%
    \begin{array}{#1}\vekSp@lten{#2}\end{array}%
  \hskip-\arraycolsep\right)}

\def\vekSp@lten#1{\xvekSp@lten#1;vekL@stLine;}
\def\vekL@stLine{vekL@stLine}
\def\xvekSp@lten#1;{\def\temp{#1}%
  \ifx\temp\vekL@stLine
  \else
    \ifnum\@VORNE=1\gdef\@VORNE{0}
    \else\@arraycr\fi%
    #1%
    \expandafter\xvekSp@lten
  \fi}
\makeatother

\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}
\newcommand*\Laplace{\mathop{}\!\mathbin\bigtriangleup}

\title{Condensed Matter Physics I - Week 7}
\subtitle{Band structure theory}
\date{Tuesday, 11$^{\text{th}}$ November 2014}

\begin{document}


\maketitle

\setlength{\unitlength}{1cm}
%\advance\textwidth by 3cm
%\advance\hoffset by -1.5cm 
\advance\textheight by 1cm
\advance\voffset by -1.5cm
\setmarginsrb{3cm}{0.5cm}{1.5cm}{1cm}{1cm}{1cm}{1cm}{1cm}
%\setlength{\parindent}{0cm}%

\pagestyle{plain}

Note: You may write your solutions in English or Czech language, at
your convenience.

\newpage

\section{Nearly free electrons in 1D}

Electrons of mass $m$ are confined in 1D, and a weak periodic
potential $V(x)$ is applied. $V(x)$ is described by its Fourier series
$V(x) = V_0  + V_1 \cos(2 \pi x / a)  + V_2 \cos(4 \pi x / a) + \ldots$

\begin{enumerate}
\item Under what conditions is the nearly free electron approximation
  valid?
\item Assuming the nearly free electron approximation is valid, sketch
  the three lowest energy bands in the first Brillouin zone.
\item Calculate (to first-order) the energy gap between the first and
  second bands at $k=\pi/a$.
\item Calculate (to first-order) the energy gap between the second and
  third bands at $k=0$.
\end{enumerate}

\newpage

\section{Nearly free electrons in 2D}

We consider a 2D nearly free electron system on a square lattice of
period $a$. The periodic potential exerted on the electrons is given
by its Fourier transform $V(\boldsymbol{K})$.

We wish to investigate the band structure around the $(\pi/a, \pi/a)$
point in the reciprocal space. We assume that the only non-zero
components of $V(\boldsymbol{K})$ are at points $\boldsymbol{K} = (0,
2\pi/a)$ and $\boldsymbol{K} = (2\pi/a, 2\pi/a)$.

You may use the following:

\begin{equation}
\label{schrod_momentum}
  \biggl[ \dfrac{\hbar^2}{2m}(\boldsymbol{k} - \boldsymbol{K})^2 -
  \epsilon \biggr] c_{\boldsymbol{k} - \boldsymbol{K}} +
  \sum_{\boldsymbol{K'}} U_{\boldsymbol{K'} -
    \boldsymbol{K}}c_{\boldsymbol{k} - \boldsymbol{K'}} = 0
\end{equation}
where $\boldsymbol{K}$ and $\boldsymbol{K'}$ are reciprocal lattice
vectors.

\begin{enumerate}
\item What is Eq.~\ref{schrod_momentum}?
\item Show that the unperturbed spectrum is four times
degenerate at the $(\pi/a, \pi/a)$
point. Why is it important, in relation to Eq.~\ref{schrod_momentum}?
\item Find the gap if $V_{(0, 2\pi/a)} = V_0$ and $V_{(2\pi/a, 2\pi/a)} = 0$
\item Find the gap if $V_{(0, 2\pi/a)} = 0$ and $V_{(2\pi/a, 2\pi/a)}
  = V_1$
\item Derive Eq.~\ref{schrod_momentum}.
\end{enumerate}

\newpage

\section{Band overlap}

Show that a band overlap is not allowed in 1D.

\newpage

\section{Instability in a 1D system}

A weak periodic potential $V(x) = V_0 \cos (2 k_F x)$ is created in a
1D electron system ($k_F$ is the Fermi vector). Calculate $E(k)$ for
the lowest energy band, and determine the total energy of the system
at zero temperature as a function of $V_0$. Discuss the physical
implications of the result.

You may use the established result, valid in the non degenerate,
nearly free electronic case, where only the Fourier component $\mathbf{G}_0$
of the potential is non zero:

\begin{equation}
E^{\pm}(k) = \dfrac{1}{2}(E_k+E_{k-G_0}) \pm \dfrac{1}{2} \sqrt{(E_k-E_{k-G_0})^2+4V^2_{G_0}}
\end{equation}

\newpage

\section{Tight binding bands: two atoms per unit cell}

Consider a one dimensional lattice with two atoms per unit cell ($A$ and $B$). 
Assume one valence orbital per atom, $\psi_{A}$ and $\psi_{B}$ for $A$
and $B$ respectively. Compute the band structure using the
tight-binding method. Consider only the following matrix elements of
the effective one particle hamiltonian $h$, while neglecting others,
and approximate the overlap matrix by the corresponding unit matrix:
\begin{enumerate}[label=(\roman*)]
\item the diagonal or ``on-site'' matrix elements:
\begin{enumerate}
\item $\matrixel{\psi_{A}(i)}{h}{\psi_{A}(i)}=\epsilon_{A}$,
\item $\matrixel{\psi_{B}(i)}{h}{\psi_{B}(i)}=\epsilon_{B}$.
\end{enumerate}
\item the ``hopping matrix elements" between nearest neighbours,
\begin{equation}
\begin{split}
\matrixel{\psi_{A}(i)}{h}{\psi_{B}(i)} = 
\matrixel{\psi_{A}(i)}{h}{\psi_{B}(i-1)} = \\
\matrixel{\psi_{B}(i)}{h}{\psi_{A}(i)} = 
\matrixel{\psi_{B}(i)}{h}{\psi_{A}(i+1)} = -t\,,
\end{split}
\end{equation}
where $t$ is real and positive.
\end{enumerate}

Compute further the density of states $D(E)$ and the projected densities of states 
$D_{A}(E)$ and $D_{B}(E)$ corresponding to the atoms $A$ and
$B$. Discuss the dependence of the results on the ratio
$(\epsilon_{A}-\epsilon_{B})/t$.

\newpage

\section{Tight binding bands in 2D, with two atoms per unit cell}

Consider a rectangular lattice with parameters $a$ and $b$ in the $x$
and $y$ directions, and two
atoms per unit cell. Atoms of kind 1 are placed at the nodes, atoms of
kind 2 are placed at position $(0.25 a, 0.5b)$. The on-site energies
are $\epsilon_1$ and $\epsilon_2$.

We assume that only 4 kinds of hoppings may occur:
\begin{itemize}
\item 1-1 hopping $t_x$ along the $x$ direction.
\item 1-1 hopping $t_y$ along the $y$ direction.
\item 2-2 hopping $t_y$ along the $y$ direction.
\item 1-2 hopping $t$ between nearest neighbor paris of atoms 1-2.
\end{itemize}

For numerical applications, we will use $t_x=1/8, t_y = 1/2, t=1$.

\begin{enumerate}
\item Construct the tight binding Hamiltonian matrix $H(k)$.
\item Plot the bands along the $k_y$ axis, to $(0, \pi/b)$, for
  $\epsilon_1 = \epsilon_2 = 0$, and for $\epsilon_1 = 4, \epsilon_2 =
  -4$
\item Plot the bands along $k_x$, from $(0,0)$ to $(\pi/a, 0)$, for
  $\epsilon_1 = \epsilon_2 = 0$.
\end{enumerate}

\end{document}
