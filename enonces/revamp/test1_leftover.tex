\input{header.tex}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{float}
\usepackage{indentfirst}
\usepackage{vmargin}
\usepackage{indentfirst}
\usepackage{titling}
\usepackage{color} 
\usepackage{siunitx}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage[backend=biber,backref=true,style=unsrt,
style=numeric-comp,block=ragged,firstinits=true]{biblatex}



\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}
\newcommand*\Laplace{\mathop{}\!\mathbin\bigtriangleup}

\title{Condensed Matter Physics I}
\subtitle{Introductory examples and Hartree-Fock theory}
\date{Tuesday, 21$^{\text{st}}$ October 2014}

\begin{document}


\maketitle

\setlength{\unitlength}{1cm}
%\advance\textwidth by 3cm
%\advance\hoffset by -1.5cm 
\advance\textheight by 1cm
\advance\voffset by -1.5cm
\setmarginsrb{3cm}{0.5cm}{1.5cm}{1cm}{1cm}{1cm}{1cm}{1cm}
%\setlength{\parindent}{0cm}%

\pagestyle{plain}


\section{2D electron gas}

Consider a set of N non-interacting electrons in a 2D square box of
side $L$, and area $A=L^2$ with periodic boundary conditions.

\begin{enumerate}[label=(\roman*)]
\item Show that the density of states is a constant equal to
\[
D(E) = \dfrac{mA}{\pi \hbar^2} = \dfrac{N}{E_F},
\]
where $E_F$ is the energy of the highest occupied state at $T=0$.
\item Show that the dependence of the chemical potential on
  temperature is given by:
\[
\mu = \beta^{-1}\log(e^{\beta E_F} - 1)
\]
where $\beta = 1/k_B T$
\end{enumerate}

\section{Specific heat}

Consider a free electron gas of density $n$ in the low temperature
limit. Assume that the electronic density of states is $g(\omega)=a
\omega^{1/2}$. The Sommerfeld expansion of the Fermi function may be
useful $\int f(\omega) h(\omega) d\omega \simeq
\int\limits_0^{\mu(T)}h(\omega) d\omega + \dfrac{\pi^2}{6}(k_B
T)^2h'(\omega)_{|\omega = \mu}$

\begin{enumerate}[label=(\roman*)]
\item Establish the expression relating $n$ and $\mu$.
\item Using the fact that $n$ does not depend on $T$, show that to
  first order in $T$,
\[
\mu(T) = E_F\left[1 -\dfrac{\pi^2}{12} \left( \dfrac{k_B T}{E_F}
  \right)^2 \right]
\]
\item Show that the energy of the system is given by
\[
U \simeq \dfrac{4a}{5} \mu^{5/2} \left[ 1+ \dfrac{5 \pi^2}{8} \left(
    \dfrac{k_B T}{\mu} \right)^2 \right]
\]
\item deduce that the specific heat at constant density is given by
\[
C = \dfrac{\pi^2 n }{2 \mu_0 } k_{B}^2 T
\]
\item alternative: compute magnetic susceptibility
\end{enumerate}

\section{Second quantization formalism}

Consider a system of fermions, interacting via the
two-particle operator
\[
H^{(2)} = \dfrac{1}{2}
\sum\limits_{ijlm}V_{ijlm}c^\dagger_jc^\dagger_mc_lc_i \, .
\]

\begin{enumerate}[label=(\roman*)]
\item Use the symmetry $V_{ijlm} = V_{lmij}$ to show that the expectation
value of $H^{(2)}$ in the state $\ket{\mu\nu} = c^\dagger_\mu c^\dagger_\nu\ket{0}$ is
\[
\matrixel{\mu\nu}{H^{(2)}}{\mu\nu} = V_{\mu\mu\nu\nu}-V_{\mu\nu\nu\mu}
\]
\end{enumerate}

\section{Hartree-Fock theory}

Consider a system of N electrons, and the Hamiltonian
\[
H=\sum\limits_{i=1}^N h(i) + \sum\limits_{i=1}^N\sum\limits_{j>i}^N
\dfrac{1}{r_{ij}}
\]
where $h(i) = -\dfrac{1}{2}\Laplace_i - \sum\limits_A
\dfrac{Z_A}{r_{iA}}$.

In Hartree-Fock theory, we consider wave functions of the form of a
single determinant formed from spin orbitals:

\[
\ket{\psi_0} = \ket{\chi_m(1)\chi_n(2) \cdots \chi_k(N)} =
\dfrac{1}{N!} \sum\limits_{n=1}^N
(-1)^{p_n} P_n \{ \chi_m(1)\chi_n(2) \cdots \chi_k(N) \}
\]
where $P_n$ is an operator that generates the $n^{\text{th}}$
permutation of the electron labels $1, 2,\cdots N$, and $p_n$ is the
number of transpositions required to obtain such
permutation. Therefore, the Hartree-Fock ground state energy is:
\begin{equation}
\label{eq:HF}
E_0 = \matrixel{\psi_0}{H}{\psi_0} =
\matrixel{\psi_0}{\sum\limits_{i=1}^N h(i)}{\psi_0} +
\matrixel{\psi_0}{\sum\limits_{i=1}^N\sum\limits_{j>i}^N
\dfrac{1}{r_{ij}}} {\psi_0}
\end{equation}

\begin{enumerate}[label=(\roman*)]
\item Show that the first term of the right hand side of Eq.~\ref{eq:HF} is
\[
\matrixel{\psi_0}{\sum\limits_{i=1}^N h(i)}{\psi_0} =
\sum\limits_{m=1}^N \matrixel{m}{h}{m}
\]
where $\matrixel{m}{h}{m} = \int dx_1\{\chi_m^*(1)\} h(1)
\{\chi_m(1)\}$
\item Show that the second term of the right hand side of
  Eq.~\ref{eq:HF} is
\[
\matrixel{\psi_0}{\sum\limits_{i=1}^N\sum\limits_{j>i}^N
\dfrac{1}{r_{ij}}} {\psi_0} = \dfrac{1}{2}
\sum\limits_{m=1}^N\sum\limits_{n \neq m}^N \braket{mn}{mn} - \braket{mn}{nm}
\]
where $\braket{kl}{mn} = \int dx_1 dx_2 \{ \chi_k^*(1) \chi_l^*(2) \}
\dfrac{1}{r_{12}} \{ \chi_m(1) \chi_n(2) \}$
\item comment on the previous results, and provide the names of both
  terms appearing in the right hand side of Eq.~\ref{eq:HF}.
\end{enumerate}

\end{document}
